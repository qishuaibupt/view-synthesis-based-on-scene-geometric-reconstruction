# Geometric Reconstruction :

This repository is used to reconstruct the scene geometric mesh model, which contains TSDF integration and Marching Cube algorithm.

## Environmental Setup:

The code is tested in:
- Python 3.6
- cv2=3.4.11.45
- numpy=1.95.5
- open3d=0.15.1

## References:
- ### Robust indoor scenes reconstruction system. https://qianyi.info/scene.html
```
@inproceedings{choi2015robust,
  title={Robust reconstruction of indoor scenes},
  author={Choi, Sungjoon and Zhou, Qian-Yi and Koltun, Vladlen},
  booktitle={Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition},
  pages={5556--5565},
  year={2015}
}
@inproceedings{zhou2014simultaneous,
  title={Simultaneous localization and calibration: Self-calibration of consumer depth cameras},
  author={Zhou, Qian-Yi and Koltun, Vladlen},
  booktitle={Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition},
  pages={454--460},
  year={2014}
}
@inproceedings{zhou2013elastic,
  title={Elastic fragments for dense scene reconstruction},
  author={Zhou, Qian-Yi and Miller, Stephen and Koltun, Vladlen},
  booktitle={Proceedings of the IEEE International Conference on Computer Vision},
  pages={473--480},
  year={2013}
}
@article{zhou2013dense,
  title={Dense scene reconstruction with points of interest},
  author={Zhou, Qian-Yi and Koltun, Vladlen},
  journal={ACM Transactions on Graphics (ToG)},
  volume={32},
  number={4},
  pages={1--8},
  year={2013},
  publisher={ACM New York, NY, USA}
}
```
