# View synthesis based on scene geometric reconstruction

  ### This repository contains the official implementation for the following paper:
- Dense view synthesis for three-dimensional light-field display based on scene geometric reconstruction

## The Generated Results (Please click the links):
- <img src="https://gitlab.com/qishuaibupt/view-synthesis-based-on-scene-geometric-reconstruction/-/raw/main/Images/Scene1.gif" width="230">

- [ ] [The generated dense views (70 views per scene).](https://drive.google.com/drive/folders/17v0lav5Wv0u_OMmVzopV5UqX5L7hBcbU?usp=sharing)

- [ ] [The reconstructed mesh model with color texture.](https://drive.google.com/drive/folders/1-LZVeFLfk9PGjzzRpG-OZv8NTeL29Xya?usp=sharing)

- [ ] [The reconstructed mesh model without color texture.](https://drive.google.com/drive/folders/1-V7a_9wwNoYR_lCNTEzMJ55qJo243wi7?usp=sharing)

- [ ] [The mesh model reconstructed by single RGB-D camera.](https://drive.google.com/drive/folders/1ke9EqgacK5REUriqDFBDbkN1Ri6b9e7-?usp=sharing)

## Environmental Setup:

The code is tested in:
- Python=3.6
- Pytorch=1.4.0
- torchvision=0.5.0
- CUDA=10.1
- cv2=3.4.11.45
- numpy=1.95.5
- open3d=0.15.1
```
cd existing_repo
git remote add origin https://gitlab.com/qishuaibupt/view-synthesis-based-on-scene-geometric-reconstruction.git
git branch -M main
git push -uf origin main
```
## Code organization: 
- ### Geometric Reconstruction :
- [ ] Geometric mesh model reconstruction. 
- [ ] Geometric mesh model reconstruction with color texture.
- ### Network:
- [ ] View synthesis network.
    
    ### We will release the code once everything is ready.  

## License
MIT

